from DataMeasurement import DataMeasurement
class Dsequence:
    def __init__(self, measurements,quality):
            self.measurements = measurements
            self.quality = quality
     
    def __repr__(self):
        return 'Dsequence(measurements= '+str(self.measurements)+', quality= '+str(self.quality) + ')'

    def __str__(self):
        return 'Dsequence(measurements= '+str(self.measurements)+', quality= '+str(self.quality) + ')'
    
    def add(self,measurement):
        self.measurements.append(measurement)
        
    def showMeasurements(self):
        return self.measurements
    
    def listofActivities(self):
        activities = []
        for act in self.measurements:
            activities.append(act.activity)
        return activities