from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import mean_squared_error
import numpy as np

class DataMeasurement:
    def __init__(self, value,activity,sq):
        self.value = value
        self.activity = activity
        self.sq = sq
        

#     def add_trick(self, trick):
#         self.tricks.append(trick)
    def __repr__(self):
        return '(value= '+str(self.value)+', activity= '+str(self.activity) +', sq='+str(self.sq)+ ')'

    def __str__(self):
        return 'DataMeasurement(value= '+str(self.value)+', activity= '+str(self.activity) +', sq='+str(self.sq)+ ')'
    
