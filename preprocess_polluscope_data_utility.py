import math
import csv
def transform_polluscope_data(readFile,writeFile,fnames=['timestamp','bc','temp','battery']):
    file = open(writeFile, 'w')
    writer = None
    with file:
        #fnames = ['timestamp','bc','temp','battery']
        writer = csv.DictWriter(file, fieldnames=fnames)
        writer.writeheader()
        with open(readFile, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                timestamp,pm,canarin_id = row['timestamp'],row['canarin_id'],row['pm2.5']
                if pm=='' or math.isnan(int(pm)) or int(pm)<0 :
                    print(pm + " is nan or negative")
                else:
                    writer.writerow({fnames[0]:row['timestamp'],
                                 fnames[1]:row['canarin_id'],
                                 fnames[2]:row['pm2.5']
                                })
